import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

class CardSection extends React.Component{
    render(){
        return(
            <View style={styles.containerStyle}>
                {this.props.children}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle:{
        borderBottomWidth: 1,
        padding:5,
        backgroundColor: '#fff',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        borderColor:'#ddd',
        position: 'relative'
    }
})

export {CardSection}