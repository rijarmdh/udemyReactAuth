import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Header, Button, Spinner} from './components/common';
import firebase from 'firebase';
import LoginForm from './components/LoginForm';


export default class App extends React.Component{
  constructor(){
    super()
    this.state={
      loggedIn: null
    }
  }

  componentWillMount(){
    try{
      firebase.initializeApp({
        apiKey: "AIzaSyDMg8SlnV1if4vpFnq9hmukduiL4FRBHZU",
        authDomain: "authenticationreact-e9268.firebaseapp.com",
        databaseURL: "https://authenticationreact-e9268.firebaseio.com",
        projectId: "authenticationreact-e9268",
        storageBucket: "authenticationreact-e9268.appspot.com",
        messagingSenderId: "546754319405"
      })
    }catch(err){
      if (!/already exists/.test(err.message)) {
        console.error('Firebase initialization error', err.stack)
        }
    }

    //ngecek apakah sudah login atau belum & jika sudah maka state loggin akan terus true meskipun aplikasi di keluarkan
    firebase.auth().onAuthStateChanged(
      (user)=>{ //user merupakan argumen yang isinya didapat dalam firebase.auth()
        if(user){this.setState({loggedIn: true})}
          else{this.setState({loggedIn:false})}
      }
    )
      
  }

  onSignIn(){
    if(this.state.loggedIn){
      return (
        <View style={styles.buttonStyle}>
          <Button  text="LogOut" onpress={()=>firebase.auth().signOut()}/>
        </View>
      )
    }else if(this.state.loggedIn === null){
      return <Spinner/>
    }
    else{
      return <LoginForm/>
    }
  }
  
  render(){
    {console.log(this.state.loggedIn)}
    return(
      <View>
        <Header headerText = 'Authentication'/>
        {this.onSignIn()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  buttonStyle:{
    height: 50
  }
})