import React from 'react';
import {View, Text, ActivityIndicator, StyleSheet} from 'react-native';

const Spinner = (props) => {
    return (
        <View style={{marginTop: 5}}>
            <ActivityIndicator size = 'small'/>
        </View>
    )
}

export {Spinner}