import React from 'react';
import {View, Text, TextInput, StyleSheet, TouchableOpacity} from 'react-native';
import {Card, CardSection, Button, Spinner} from './common';
import firebase from 'firebase';

export default class LoginForm extends React.Component{
    constructor(){
        super()
        this.state = {
            email:'',
            password:'',
            error:'',
            loading: false

        }
    }
    onButton(){

        this.setState({error:'', loading:true})

        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(
                ()=>this.setState({error:'login success !', loading:false})
            
            )
            .catch(
                ()=>firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
                        .then(this.onSignUpSuccess.bind(this))
                        .catch(
                            (err)=> this.setState({error:err.code, loading:false})
                        )
            )

    }

    onSignUpSuccess(){
        this.setState({error:'Sign Up Success !', loading:false})
    }

    onRenderingSpinner(){
        if(this.state.loading){
            return <Spinner/>
        }

        return <Button text="Log In" onpress = {this.onButton.bind(this)}/>

    }


    render(){
        
        {console.log(this.state.error)}
        return(
            <Card>
                <CardSection>
                    <Text style={styles.textstyle}>Email  :</Text>
                    <TextInput
                        value = {this.state.email}
                        style={{flex:1, lineHeight:30}}
                        placeholder="user@gmail.com"
                        onChangeText={(e)=> this.setState({email:e})}
                    />
                </CardSection>
               
                <CardSection>
                    <Text style={styles.textstyle}>Password  :  </Text>
                    <TextInput
                        secureTextEntry = {true}
                        textBreakStrategy = 'highQuality'
                        style={{flex:1, color:"#000"}}
                        value={this.state.password}
                        placeholder=" 123456"
                        onChangeText={(e)=>this.setState({password:e})}
                        />
                </CardSection>

                <CardSection>
                    {this.onRenderingSpinner()}
                </CardSection>

                 <Text style={{alignSelf: 'center', }}>{this.state.error}</Text>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    textstyle:{
        width:100,
        marginLeft: 7,
        fontSize: 16,
        color:'black'
    }
})